import { assert } from "chai";
import { AddressValueError } from "../src";

const TEST_ADDRESS = "127.0.0.1";

describe("AddressValueError", function () {
    it("should be subclass of Error", function () {
        try {
            throw new AddressValueError(TEST_ADDRESS);
        } catch (exception) {
            assert.instanceOf(exception, Error);
        }
    });
    it("should have stack trace", function () {
        try {
            throw new AddressValueError(TEST_ADDRESS);
        } catch (exception) {
            assert.property(exception, "stack");
        }
    });
    it("should include the correct string", function () {
        try {
            throw new AddressValueError(TEST_ADDRESS);
        } catch (exception) {
            assert.equal(
                (exception as Error).toString(),
                `Error: Invalid address: ${TEST_ADDRESS}`
            );
        }
    });
});

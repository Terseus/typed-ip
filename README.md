[![pipeline status](https://gitlab.com/Terseus/typed-ip/badges/master/pipeline.svg)](https://gitlab.com/Terseus/typed-ip/-/commits/master)
[![coverage report](https://gitlab.com/Terseus/typed-ip/badges/master/coverage.svg)](https://gitlab.com/Terseus/typed-ip/-/commits/master) 
[![npm](https://img.shields.io/npm/v/typed-ip.svg)](https://www.npmjs.com/package/typed-ip)

# typed-ip

A Typescript library with zero dependencies to parse and manipulate IP addresses.

This project is **NOT** ready for production right now, you may expect changes to the public interfaces.


## Documentation

Documentation is available at the [project Gitlab pages](https://terseus.gitlab.io/typed-ip/).


## Installing

```shell
$ npm install typed-ip
```


## Building

Before starting, install the dependencies:

```shell
$ yarn install
```


To build the project:

```shell
$ npm run build
```


To create an installable package:

```shell
$ npm run pack
```


To run the tests directly in Typescript (for development):
```shell
$ npm run test
```


To run the tests in Javascript (includes coverage):

```shell
$ npm run test-js
```
